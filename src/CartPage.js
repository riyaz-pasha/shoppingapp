import React, { Component } from 'react';
import CheckoutItem from "./components/CheckoutItem"
import {getCartItems} from "./components/CartService"
class CartPage extends Component {
    componentDidMount(){
        window.addEventListener("cart-is-modified",()=>{this.forceUpdate()})
    } 
    render() {
        return (
            <div>  
                <ul>
                    {getCartItems().map((selected)=><CheckoutItem key={selected.item.id} selectedItem={selected}/>)}
                </ul>
                -------------------------------
               Total Amount to Pay :{getCartItems().reduce((acc,currItem)=>{
                 return acc+(currItem.item.phonePrice*currItem.quantity)
             },0)}
            </div>
        );
    }
}

export default CartPage;