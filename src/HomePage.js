import React, { Component } from "react";
import ReactDOM from "react-dom";

import Phones from "./components/Phones";
import Cart from "./components/Cart";
import CartPage from "./CartPage";

class HomePage extends Component {

  nextPage = () => {
    return ReactDOM.render(
      <CartPage/>,
      document.getElementById("root")
    );
  };
  render() {
    return (
      <div>
        <Cart/>
        <Phones  />
        <button onClick={this.nextPage}>Submit</button>
      </div>
    );
  }
}
export default HomePage;
