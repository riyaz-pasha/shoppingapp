import React, { Component } from "react";
import { getCartSize } from "../components/CartService";

class Cart extends Component {
  constructor(props) {
    super(props);
    window.addEventListener("cart-is-modified", this.getUpadatedCartSize);
  }
  componentWillUnmount(){
    window.removeEventListener("cart-is-modified",this.getUpadatedCartSize);
  }
  getUpadatedCartSize=()=>{
    this.forceUpdate();
  }
  render() {
    return (
      <div>
        <p>In Cart : {getCartSize()}</p>
      </div>
    );
  }
}
export default Cart;
