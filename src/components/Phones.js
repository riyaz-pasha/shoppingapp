import React, { Component } from "react";
import Phone from "./Phone";
import "../css/Phones.css";

const listOfPhones = [
  {
    id: 1,
    phoneName: "Vivo V15",
    phoneImagePath: "./Images/VivoV15.jpg",
    phonePrice: 20000
  },
  {
    id: 2,
    phoneImagePath: "./Images/Honor 8C.jpg",
    phoneName: "Honor 8C",
    phonePrice: 8999
  },
  {
    id: 3,
    phoneImagePath: "./Images/Honor9N.jpg",
    phoneName: "Honor 9N",
    phonePrice: 8999
  },
  {
    id: 4,
    phoneImagePath: "./Images/OppoA7.jpg",
    phoneName: "Oppo A7",
    phonePrice: 8999
  },
  {
    id: 5,
    phoneImagePath: "./Images/OppoK3.jpg",
    phoneName: "Oppo K3",
    phonePrice: 8999
  },
  {
    id: 6,
    phoneImagePath: "./Images/Redmi7.jpg",
    phoneName: "Redmi 7",
    phonePrice: 8999
  },
  {
    id: 7,
    phoneImagePath: "./Images/RedmiY2.jpg",
    phoneName: "Redmi Y2",
    phonePrice: 8999
  },
  {
    id: 8,
    phoneImagePath: "./Images/SamsungGalaxyM10.jpg",
    phoneName: "Samsung Galaxy M10",
    phonePrice: 8999
  },
  {
    id: 9,
    phoneImagePath: "./Images/SamsungGalaxyM20.jpg",
    phoneName: "Samsung Galaxy M20",
    phonePrice: 8999
  },
  {
    id: 10,
    phoneImagePath: "./Images/SamsungGalaxyM20.jpg",
    phoneName: "Samsung Galaxy M20",
    phonePrice: 8999
  },
  {
    id: 11,
    phoneImagePath: "./Images/Nokia6.jpg",
    phoneName: "Nokia 6",
    phonePrice: 8999
  }
];
class Phones extends Component {
  render() {
    return (
      <div className="AllPhones">
        {listOfPhones.map(phone => (
          <Phone key={phone.id} phone={phone}/>
        ))}
      </div>
    );
  }
}
export default Phones;
