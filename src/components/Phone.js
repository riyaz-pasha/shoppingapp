import React, { Component } from "react";
import { addPhoneToCart } from "../components/CartService";
import "../css/Phone.css";

class Phone extends Component {
  render() {
    return (
      <div className="Phone">
        <img
          src={require(`${this.props.phone.phoneImagePath}`)}
          alt={this.props.phone.phoneName}
        />
        <p className="PhoneName">{this.props.phone.phoneName}</p>
        <p className="PhonePrice">{this.props.phone.phonePrice}</p>
        <button
          name="clickToAddToCart"
          onClick={() => {
            addPhoneToCart(this.props.phone);
          }}
        >
          add to cart {this.props.phone.phoneName}
        </button>
      </div>
    );
  }
}
export default Phone;
