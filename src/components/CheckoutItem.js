import React, { Component } from 'react';
import {increaseQuantity,decreaseQuantity} from "../components/CartService"

class CheckoutItem extends Component {
    render() {
        const {selectedItem}=this.props
        const {item,quantity}=selectedItem
        return (
            <>
            <li>{item.phoneName}  --- Price : {item.phonePrice} --- Item Total Price : {item.phonePrice*quantity} 
            <button onClick={()=>increaseQuantity(item)}>+1</button> {quantity} <button onClick={()=>decreaseQuantity(item)}>-1</button>
            </li> 
            </>
        );
    }
}

export default CheckoutItem;