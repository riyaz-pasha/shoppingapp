var addToCart = [];

const addPhoneToCart = receivedItem => {
  const itemInCart = addToCart.find(element => element.item.id === receivedItem.id)
  itemInCart?itemInCart.quantity++:(addToCart=addToCart.concat({ item: receivedItem, quantity: 1 }));
  cartIsModifiedEvent()
}

const cartIsModifiedEvent=()=>{
  let event = new Event("cart-is-modified");
  window.dispatchEvent(event);
}
const decreaseQuantity=(receivedItem) => {
  const index = addToCart.findIndex(element => element.item.id === receivedItem.id);
  if(addToCart[index].quantity>0){
    addToCart[index].quantity--;}
  cartIsModifiedEvent();
}
const increaseQuantity=(receivedItem) => {
  addToCart.find(element => element.item.id === receivedItem.id).quantity++;
  cartIsModifiedEvent();
}

const getCartSize = () => addToCart.length;

const getCartItems = () => addToCart;

export { addPhoneToCart, getCartItems, getCartSize,increaseQuantity,decreaseQuantity };
